import math

import numpy as np
from numpy.linalg import inv
from random import choice, randint
import matplotlib.pyplot as plt

C = np.array([
    [19, 7, 3],
    [6, 9, 9],
    [8, 2, 11]
])
# C = np.array([
#     [2, 1, 3],
#     [3, 0, 1],
#     [1, 2, 1]
# ])



def solve_analytic(C, toprint=True):
    C_inv = inv(C)
    u = np.ones(C.shape[0])

    x_star = (u.dot(C_inv)) / (u.dot(C_inv).dot(u.transpose()))
    y_star = (C_inv.dot(u.transpose())) / (u.dot(C_inv).dot(u.transpose()))
    v = 1 / (u.dot(C_inv).dot(u.transpose()))
    if toprint:
        print('x* = %s, y* = %s, v = %.2f' % (x_star, y_star, v))
    return x_star, y_star, v


def indices(l, x):
    i = 0
    result = []
    while True:
        try:
            i = l.index(x, i)
        except ValueError:
            return result
        result.append(i)
        i += 1

class ColumnOutput:

    def __init__(self):
        self.columns = {}
        self.lines = []
        self.line = []

    def put(self, x):
        i = len(self.line)
        if i not in self.columns:
            self.columns[i] = 0

        self.columns[i] = max(self.columns[i], len(str(x)))
        self.line.append(str(x))

    def newline(self):
        self.lines.append(self.line)
        self.line = []


    def output(self):
        fs = {i: '%' + str(x) + 's' for i, x in self.columns.items()}

        for line in self.lines:
            for i, word in enumerate(line):
                print(fs[i] % word, end='    ')
            print()

def unwrap(d, is_array=False):
    if is_array:
        (a1, a2, a3), (b1, b2, b3) = [list(zip(*x)) for x in zip(*d)]
    else:
        (a1, a2, a3), (b1, b2, b3) = d
    return a1, a2, a3, b1, b2, b3

def solve_brownrobinson(C):
    co = ColumnOutput()
    xi = randint(0, C.shape[0] - 1)
    yi = randint(0, C.shape[0] - 1)

    X = np.zeros_like(C[0])
    Y = np.zeros_like(C[0])
    min_kvx = 0xFFFF_FFFF
    max_kvy = 0

    signs = 2
    e_sign = 10 ** -signs
    f_format = '%.' + str(math.ceil(signs)) + 'f'

    cnt_X = np.zeros_like(C[0])
    cnt_Y = np.zeros_like(C[0])

    co.put('i')
    co.put('xi')
    co.put('yi')
    co.put('x1 x2 x3')
    co.put('y1 y2 y3')
    co.put('~v[k]/k')
    co.put('v~[k]/k')
    co.put('min ~v[k]/k')
    co.put('max v~[k]/k')
    co.put('eps')
    co.newline()

    plt_data = []
    plt_v = []
    d1 = []
    d2 = []

    e = 1.0
    i = 0
    while e > e_sign:
        d1.append(xi)
        d2.append(yi)

        X += C.transpose()[yi]
        max_x = max(X)
        x_inds = indices(list(X.tolist()), max_x)
        xin = choice(x_inds)

        kvx_i = max_x/(i + 1)
        min_kvx = min(min_kvx, kvx_i)
        cnt_X[xi] += 1

        Y += C[xi]
        min_y = min(Y)
        y_inds = indices(list(Y.tolist()), min_y)
        yin = choice(y_inds)
        kvy_i = min_y/(i + 1)
        max_kvy = max(max_kvy, kvy_i)
        cnt_Y[yi] += 1

        e = min_kvx - max_kvy

        co.put(i + 1)
        co.put(xi + 1)
        co.put(yi + 1)
        co.put(X)
        co.put(Y)
        co.put(f_format % kvx_i)
        co.put(f_format % kvy_i)
        co.put(f_format % min_kvx)
        co.put(f_format % max_kvy)
        co.put(f_format % e)
        co.newline()

        xi = xin
        yi = yin
        i += 1
        plt_data.append((cnt_X / i, cnt_Y / i))
        plt_v.append((min_kvx, max_kvy, (min_kvx + max_kvy) / 2))

    co.output()
    print('x* =', cnt_X / i, 'y* = ', cnt_Y / i)

    kv = (min_kvx + max_kvy) / 2
    print('v =', f_format % kv)

    x1, x2, x3, y1, y2, y3 = unwrap(plt_data, True)
    ax, ay, av = solve_analytic(C, False)
    ax1, ax2, ax3, ay1, ay2, ay3 = unwrap((ax, ay))

    plt.figure()
    plt.plot(x1, label='x*1 численное')
    plt.plot(x2, label='x*2 численное')
    plt.plot(x3, label='x*3 численное')
    plt.plot([ax1]*len(x1), label='x*1 аналитическое')
    plt.plot([ax2]*len(x2), label='x*2 аналитическое')
    plt.plot([ax3]*len(x3), label='x*3 аналитическое')
    plt.legend()
    plt.show()

    plt.figure()
    plt.plot(y1, label='y*1 численное')
    plt.plot(y2, label='y*2 численное')
    plt.plot(y3, label='y*3 численное')
    plt.plot([ay1]*len(y1), label='y*1 аналитическое')
    plt.plot([ay2]*len(y2), label='y*2 аналитическое')
    plt.plot([ay3]*len(y3), label='y*3 аналитическое')
    plt.legend()
    plt.show()

    # График цены игры не впечатляет
    # min_v, max_v, avg_v = list(zip(*plt_v))
    #
    # plt.figure()
    # plt.plot(min_v, label='v численное минимальное')
    # plt.plot(max_v, label='v численное максимальное')
    # plt.plot(avg_v, label='v численное среднее')
    # plt.plot([av]*len(avg_v), label='v аналитическое')
    # plt.legend()
    # plt.show()



solve_brownrobinson(C)
solve_analytic(C)
